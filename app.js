const rp = require('request-promise');
// const request = require('request');
const cheerio = require('cheerio');
// Это ты не юзаешь?
// const Nightmare = require('nightmare');
// const nightmare = Nightmare({ show: true });


async function load(url) {
    try {
        const body = await rp({ url })
        const $ =  cheerio.load(body)
        
        // зачем тут await?
        mainHtml = $('#scoreboardElement').html()
        
        return mainHtml;
    } catch (error) {
        console.log("Произошла ошибка: " + error)
        // console.dir(error)
        throw error
    }
}


async main() {
    let url = 'https://www.hltv.org/matches/2341268/secret-vs-big-home-sweet-home-cup-3'
    const html = await load(url)
    
    console.log(html)
}

main()